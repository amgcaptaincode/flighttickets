package com.amg.flighttickets.presenter;

import com.amg.flighttickets.CallbackWrapper;
import com.amg.flighttickets.network.ApiClient;
import com.amg.flighttickets.network.ApiService;
import com.amg.flighttickets.network.model.Ticket;
import com.amg.flighttickets.presenter.interfaces.InterfaceFlightTickerPresenter;
import com.amg.flighttickets.presenter.interfaces.InterfaceFlightTickerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;

public class FlightTicketPresenter implements InterfaceFlightTickerPresenter {

    private InterfaceFlightTickerView view;

    private CompositeDisposable disposable = new CompositeDisposable();
    private ApiService apiService;
    private ArrayList<Ticket> ticketsList = new ArrayList<>();

    public FlightTicketPresenter(InterfaceFlightTickerView view) {
        this.view = view;
        apiService = ApiClient.getClient().create(ApiService.class);
    }

    @Override
    public void getTickets(String from, String to) {

        ConnectableObservable<List<Ticket>> ticketsObservable = searchTickets(from, to).replay();

        /*
         * Fetching all tickets first
         * Observable emits List<Ticket> at once
         * All the items will be added to RecyclerView
         * */

        disposable.add(
                ticketsObservable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new CallbackWrapper<List<Ticket>>(view) {
                            @Override
                            protected void onNextCustom(List<Ticket> tickets) {
                                // Refreshing list
                                ticketsList.clear();
                                ticketsList.addAll(tickets);
                                view.showTickets(ticketsList);
                            }
                        })
        );

        /*
         * Fetching individual ticket price
         * First FlatMap converts single List<Ticket> to multiple emissions
         * Second FlatMap makes HTTP call on each Ticket emissions
         * */

        disposable.add(
                ticketsObservable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        /*
                         * Converting List<Ticket> emission to single Ticket emissions
                         * */
                        .flatMap(Observable::fromIterable)
                        /*
                         * Fetching prince on each Ticket emissions
                         * */
                        .flatMap(this::getPriceObservable)
                        .subscribeWith(new CallbackWrapper<Ticket>(view) {
                            @Override
                            protected void onNextCustom(Ticket ticket) {
                                int position = ticketsList.indexOf(ticket);
                                if (position == -1) {
                                    // TODO - take action
                                    // Ticket not found in the list
                                    // This shouldn't happen
                                    return;
                                }
                                view.updateTicketPrice(position, ticket);
                                ticketsList.set(position, ticket);
                            }
                        })
        );

        // Calling connect to start emission
        ticketsObservable.connect();

    }


    /*
     * Making Retrofit call to fetch all tickets*/

    private Observable<List<Ticket>> searchTickets(String from, String to) {
        return apiService.searchTickets(from, to)
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /*
     * Making Retrofit call to get single ticket price
     * get price HTTP call return Price object,
     * but map() operator is used to change the return type to Ticket
     */

    private Observable<Ticket> getPriceObservable(final Ticket ticket) {
        return apiService
                .getPrice(ticket.getFlightNumber(), ticket.getFrom(), ticket.getTo())
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(price -> {
                    ticket.setPrice(price);
                    return ticket;
                });
    }

    @Override
    public void onDestroy() {
        disposable.dispose();

    }
}
