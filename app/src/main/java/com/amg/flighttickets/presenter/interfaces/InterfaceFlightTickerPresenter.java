package com.amg.flighttickets.presenter.interfaces;

public interface InterfaceFlightTickerPresenter {

    void getTickets(String from, String to);

    void onDestroy();

}

