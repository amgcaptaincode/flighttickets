package com.amg.flighttickets.presenter.interfaces;

import com.amg.flighttickets.network.model.Ticket;

import java.util.List;


public interface InterfaceFlightTickerView {

    void showTickets(List<Ticket> tickets);

    void updateTicketPrice(int position, Ticket ticket);

    void showError(Throwable e);

}
