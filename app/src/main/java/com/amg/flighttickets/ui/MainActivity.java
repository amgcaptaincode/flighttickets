package com.amg.flighttickets.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.amg.flighttickets.R;
import com.amg.flighttickets.databinding.ActivityMainBinding;
import com.amg.flighttickets.network.model.Ticket;
import com.amg.flighttickets.presenter.FlightTicketPresenter;
import com.amg.flighttickets.presenter.interfaces.InterfaceFlightTickerView;
import com.amg.flighttickets.ui.adapters.TicketsAdapter;
import com.amg.flighttickets.utils.GridSpacingItemDecoration;
import com.amg.flighttickets.utils.Toolbox;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class MainActivity extends AppCompatActivity implements TicketsAdapter.TicketsAdapterListener, InterfaceFlightTickerView {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String from = "DEL";
    private static final String to = "HYD";

    private ActivityMainBinding binding;
    private TicketsAdapter mAdapter;

    private FlightTicketPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        presenter = new FlightTicketPresenter(this);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(from + " > " + to);


        mAdapter = new TicketsAdapter(this, this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        binding.contentMain.rvTickets.setLayoutManager(mLayoutManager);
        binding.contentMain.rvTickets.addItemDecoration(new GridSpacingItemDecoration(1, Toolbox.dpToPx(getBaseContext(), 5), true));
        binding.contentMain.rvTickets.setItemAnimator(new DefaultItemAnimator());
        binding.contentMain.rvTickets.setAdapter(mAdapter);

        presenter.getTickets(from, to);

    }


    @Override
    public void onTicketSelected(Ticket contact) {

    }

    @Override
    public void showTickets(List<Ticket> tickets) {
        mAdapter.setTicketList(tickets);
    }

    @Override
    public void updateTicketPrice(int position, Ticket ticket) {
        mAdapter.updateTicketPrice(position, ticket);
    }

    /**
     * Snackbar shows observer error
     */
    @Override
    public void showError(Throwable e) {
        Log.e(TAG, "showError: " + e.getMessage());

        Snackbar snackbar = Snackbar
                .make(binding.coordinatorLayout, e.getMessage(), Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }


}