package com.amg.flighttickets.ui.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amg.flighttickets.R;
import com.amg.flighttickets.databinding.TicketRowBinding;
import com.amg.flighttickets.network.model.Ticket;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.TicketsViewHolder> {

    private Context context;
    private List<Ticket> ticketList;
    private TicketsAdapterListener listener;

    public TicketsAdapter(Context context, TicketsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.ticketList = new ArrayList<>();
    }

    @NonNull
    @Override
    public TicketsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_row, parent, false);
        return new TicketsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketsViewHolder holder, int position) {
        holder.bind(ticketList.get(position));
    }

    @Override
    public int getItemCount() {
        return ticketList != null ? ticketList.size() : 0;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList.clear();
        this.ticketList.addAll(ticketList);
        notifyDataSetChanged();
    }

    public void updateTicketPrice(int position, Ticket ticket) {
        this.ticketList.set(position, ticket);
        notifyItemChanged(position);
    }

    public class TicketsViewHolder extends RecyclerView.ViewHolder {

        private TicketRowBinding binding;

        public TicketsViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = TicketRowBinding.bind(itemView);
        }

        void bind(Ticket ticket) {
            binding.getRoot().setOnClickListener(v -> listener.onTicketSelected(ticket));

            Glide.with(context)
                    .load(ticket.getAirline().getLogo())
                    .apply(RequestOptions.circleCropTransform())
                    .into(binding.logo);

            binding.airlineName.setText(ticket.getAirline().getName());
            binding.departure.setText(context.getString(R.string.label_dep, ticket.getDeparture()));
            binding.arrival.setText(context.getString(R.string.label_arrival, ticket.getArrival()));

            binding.duration.setText(ticket.getFlightNumber());
            binding.duration.append(", " + ticket.getDuration());
            binding.numberOfStops.setText(context.getString(R.string.label_stops, ticket.getNumberOfStops()));

            if (!TextUtils.isEmpty(ticket.getInstructions())) {
                binding.duration.append(", " + ticket.getInstructions());
            }

            if (ticket.getPrice() != null) {
                //binding.price.setText(context.getText(R.string.label_price, ticket.getPrice().getPrice()));
                binding.price.setText("₹" + String.format("%.0f", ticket.getPrice().getPrice()));
                binding.numberOfSeats.setText(context.getString(R.string.label_seats, ticket.getPrice().getSeats()));
                //binding.loader.setVisibility(View.INVISIBLE);
            } else {
                //binding.loader.setVisibility(View.VISIBLE);
            }

        }

    }

    public interface TicketsAdapterListener {
        void onTicketSelected(Ticket contact);
    }

}
