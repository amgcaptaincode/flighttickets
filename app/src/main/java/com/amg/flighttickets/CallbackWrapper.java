package com.amg.flighttickets;

import com.amg.flighttickets.presenter.interfaces.InterfaceFlightTickerView;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;

public abstract class CallbackWrapper<T> extends DisposableObserver<T> {

    private InterfaceFlightTickerView view;

    public CallbackWrapper(InterfaceFlightTickerView view) {
        this.view = view;
    }

    @Override
    public void onNext(@NonNull T t) {
        onNextCustom(t);
    }

    @Override
    public void onError(@NonNull Throwable e) {
        view.showError(e);
    }

    @Override
    public void onComplete() {

    }

    protected abstract void onNextCustom(T t);

}
